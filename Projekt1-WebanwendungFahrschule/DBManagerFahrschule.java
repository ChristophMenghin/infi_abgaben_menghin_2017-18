package Demo;
import java.sql.ResultSet;
import java.sql.Statement;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;


public class DBManagerFahrschule {
	
	private static Connection con;
	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) throws SQLException, IOException {
		new DBManagerFahrschule();
		while (true) {
			System.out.println("Was wollen sie machen?");
			System.out.println("Daten einf�gen......insertFahrstunde");
			System.out.println("Daten einf�gen......insertFahrschueler");
			System.out.println("Daten einf�gen......insertPraxispruefung");
			System.out.println("Daten loeschen......delete");
			System.out.println("Daten anzeigen......show");

			
			String st = scan.next();

			if (st.equals("insertFahrstunde")) {
				insertFahrstunde();
			}
			if (st.equals("insertFahrschueler")) {
				insertFahrschueler();
			}
			if (st.equals("insertPraxispruefung")) {
				insertPraxispruefung();
			}
			if (st.equals("delete")) {
				delete();
			}
			if (st.equals("show")) {
				show();
			}
		}
}
	DBManagerFahrschule() throws SQLException {
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\Christoph\\FahrschuleWeb15;foreign keys=true;");

			create();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static void create() throws SQLException {
		
		String table1 = "CREATE TABLE IF NOT EXISTS Fahrstunde(FahrstundeID INT PRIMARY KEY," + "Datum date NOT NULL,"
				+ "Uhrzeit TEXT NOT NULL," + " Dauer INT NOT NULL," + "FS_SchuelerID INT NOT NULL," + " FOREIGN KEY (FS_SchuelerID) REFERENCES Fahrschueler(SchuelerID));";
				
		String table2 = "CREATE TABLE IF NOT EXISTS Fahrschueler(SchuelerID INT PRIMARY KEY,"
				+ "Nachname TEXT NOT NULL," + "Vorname TEXT NOT NULL," + " Geburtsdatum date NOT NULL,"
				+ "Telefonnummer TEXT NOT  NULL, " + "Ort TEXT NOT NULL, " + "Stra�e TEXT NOT NULL,"
				+ "Hausnummer INT NOT NULL," + "Anzahlung INT NOT NULL);";
		
		String table3 = "CREATE TABLE IF NOT EXISTS Praxispruefung(PruefungID INT PRIMARY KEY," + "Datum date NOT NULL,"
				+ "Uhrzeit Text NOT NULL);";
		
		String zwtable = "CREATE TABLE IF NOT EXISTS Praxispruefung_Fahrschueler(FS_PruefungID INT NOT NULL,"
		+ " FS_SchuelerID INT NOT NULL,"
		+ " FOREIGN KEY (FS_SchuelerID) REFERENCES Fahrschueler(SchuelerID)"
		+ " FOREIGN KEY (FS_PruefungID) REFERENCES Praxispruefung(PruefungID));";

		PreparedStatement stmt = null;
		stmt = con.prepareStatement(table1);
		
		stmt.executeUpdate();

		stmt = con.prepareStatement(table2);
		stmt.executeUpdate();

		stmt = con.prepareStatement(table3);
		stmt.executeUpdate();
		
		stmt = con.prepareStatement(zwtable);
		stmt.executeUpdate();
		
		stmt.close();

	}
	
	public static void insertFahrstunde() throws SQLException, IOException
	{
		String csvFile = "Fahrstunden.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
		
        br = new BufferedReader(new FileReader(csvFile));
        
		PreparedStatement stmt = null;
		PreparedStatement stmtm = null;
		
		String insert1 = "INSERT INTO Fahrstunde VALUES (?,?,?,?,?)";
		        try {

		        	while ((line = br.readLine()) != null) {
		            	int i = 0;
		                String[] Wert = line.split(cvsSplitBy);
		                
		                stmt = con.prepareStatement(insert1);
		            	
		                String delete1 = "DELETE FROM Fahrstunde WHERE FahrstundeID = " + Wert[0];
		                
		                stmtm = con.prepareStatement(delete1);
		                stmtm.executeUpdate();
		                
		                stmt.setString(1, Wert[i]);
		                stmt.setString(2, Wert[i+1]);
		                stmt.setString(3, Wert[i+2]);
		                stmt.setString(4, Wert[i+3]);
		                stmt.setString(5, Wert[i+4]);
		                System.out.println( Wert[i] + Wert[i+1] + Wert[i+2] + Wert[i+3] + Wert[i+4]);
		                stmt.executeUpdate();
		            }
		        } catch (FileNotFoundException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        } finally {
		            if (br != null) {
		                try {
		                    br.close();
		                } catch (IOException e) {
		                    e.printStackTrace();
		                }
		            }
		        }
		        System.out.println();
	}
	
	public static void insertFahrschueler() throws SQLException, IOException
	{
		String csvFile = "Fahrschueler.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
		
        br = new BufferedReader(new FileReader(csvFile));
        
		PreparedStatement stmt = null;
		PreparedStatement stmtm = null;
		
		String insert1 = "INSERT INTO Fahrschueler VALUES (?,?,?,?,?,?,?,?,?)";
		        try {

		        	while ((line = br.readLine()) != null) {
		            	int i = 0;
		              
		                String[] Wert = line.split(cvsSplitBy);
		                
		                stmt = con.prepareStatement(insert1);
		                
		                
		                String delete1 = "DELETE FROM Fahrschueler WHERE SchuelerID = " + Wert[0];
		                
		                stmtm = con.prepareStatement(delete1);
		                stmtm.executeUpdate();
		                
		                stmt.setString(1, Wert[i]);
		                stmt.setString(2, Wert[i+1]);
		                stmt.setString(3, Wert[i+2]);
		                stmt.setString(4, Wert[i+3]);
		                stmt.setString(5, Wert[i+4]);
		                stmt.setString(6, Wert[i+5]);
		                stmt.setString(7, Wert[i+6]);
		                stmt.setString(8, Wert[i+7]);
		                stmt.setString(9, Wert[i+8]);
		                System.out.println( Wert[i] + Wert[i+1] + Wert[i+2] + Wert[i+3] + Wert[i+4]+ Wert[i+5]+ Wert[i+6]+ Wert[i+7]+ Wert[i+8]);
		                stmt.executeUpdate();
		            }
		        } catch (FileNotFoundException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        } finally {
		            if (br != null) {
		                try {
		                    br.close();
		                } catch (IOException e) {
		                    e.printStackTrace();
		                }
		            }
		        }
		        System.out.println();
	}
	
	public static void insertPraxispruefung() throws SQLException, IOException
	{
		String csvFile = "Praxispruefung.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";
		
        br = new BufferedReader(new FileReader(csvFile));
        
		PreparedStatement stmt = null;
		PreparedStatement stmtm = null;
		
		String insert1 = "INSERT INTO Praxispruefung VALUES (?,?,?)";
		        try {

		        	while ((line = br.readLine()) != null) {
		            	int i = 0;
		              
		                String[] Wert = line.split(cvsSplitBy);
		                
		                stmt = con.prepareStatement(insert1);
		                
		                
		                String delete1 = "DELETE FROM Praxispruefung WHERE PruefungID = " + Wert[0];
		                
		                stmtm = con.prepareStatement(delete1);
		                stmtm.executeUpdate();
		                
		                stmt.setString(1, Wert[i]);
		                stmt.setString(2, Wert[i+1]);
		                stmt.setString(3, Wert[i+2]);
		                System.out.println( Wert[i] + Wert[i+1] + Wert[i+2]);
		                stmt.executeUpdate();
		            }
		        } catch (FileNotFoundException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		        } finally {
		            if (br != null) {
		                try {
		                    br.close();
		                } catch (IOException e) {
		                    e.printStackTrace();
		                }
		            }
		        }
		        System.out.println();
	}

	
	public static void show() throws SQLException {
		System.out.println("Von welcher Tabelle sollen die Daten ausgegeben werden? ");
		String st = scan.next();
		String sql = "SELECT * FROM " + st + ";";

		Statement stmt = con.createStatement();
		ResultSet rs = stmt.executeQuery(sql);

		while (rs.next()) {
			if (st.equals("Fahrstunde")) {
				System.out.println("FahrstundeID: " + rs.getInt(1));
				System.out.println("Datum: " + rs.getString(2));
				System.out.println("Uhrzeit: " + rs.getString(3));
				System.out.println("Dauer: " + rs.getString(4));
			}
			if (st.equals("Fahrschueler")) {
				System.out.println("SchuelerID: " + rs.getInt(1));
				System.out.println("Vorname: " + rs.getString(2));
				System.out.println("Nachname: " + rs.getString(3));
				System.out.println("Geburtsdatum: " + rs.getString(4));
				System.out.println("Telefonnummer: " + rs.getInt(5));
				System.out.println("Ort: " + rs.getString(6));
				System.out.println("Stra�e: " + rs.getString(7));
				System.out.println("Hausnummer: " + rs.getInt(8));
				System.out.println("Anzahlung: " + rs.getInt(9));
			}
			if (st.equals("Praxispruefung")) {
				System.out.println("PruefungID: " + rs.getInt(1));
				System.out.println("Datum: " + rs.getString(2));
				System.out.println("Uhrzeit: " + rs.getString(3));
			}
			if (st.equals("Praxispruefung_Fahrschueler")) {
				System.out.println("Sch�lerID: " + rs.getInt(1));
				System.out.println("Praxispr�fungID: " + rs.getInt(2));
			System.out.println();
			}
		}

		rs.close();
		stmt.close();
		System.out.println();
	}
	
	public static void delete() {
		PreparedStatement stmt = null;
		System.out.println("Welche Tabelle? ");
		String st = scan.next();
	
		String delete1 = "DELETE FROM  " + st;
		String delete2 = "DELETE FROM  " + st;
		String delete3 = "DELETE FROM  " + st;

		try {
			if (st.equals("Fahrstunde")){
				
				stmt = con.prepareStatement(delete1);
				stmt.executeUpdate();
		}
			if (st.equals("Fahrschueler")) {
				stmt = con.prepareStatement(delete2);
				stmt.executeUpdate();
				
			}
			if (st.equals("Praxispruefung")) {
				stmt = con.prepareStatement(delete3);
				stmt.executeUpdate();

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println();
	}
	
	}